/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agep;

import java.awt.Desktop;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import static java.awt.print.Printable.PAGE_EXISTS;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.sl.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Christian
 */
public class Nacimiento_consul extends javax.swing.JFrame implements Printable {
  public   Vector<DatosC> datos2 ;
  public Vector<Integer> idsN=new Vector<Integer>();
public Vector<Integer> idsM=new Vector<Integer>();
public Vector<Integer> idsD=new Vector<Integer>();
//public Vector<Integer> idsrepetidos=new Vector<Integer>();

DefaultTableModel modeloT1,modeloT2,modeloT3,modeloT4;

private static Conexion conexion;
  public   Vector<String> dads= new Vector<String>();            
        public   Vector<String> mat= new Vector<String>();    
        public Vector<String> famat=new Vector<String>();
        public Vector<String> famdef=new Vector<String>();
       public Vector<String> Vfamdef=new Vector<String>();
        public Vector<String> testigoss=new Vector<String>();
        
     /**
     * 
     * Creates new form Nacimiento_consul
     */

    public Nacimiento_consul(Vector<DatosC> datos2,Vector<Integer> idsN,Vector<Integer> idsM,Vector<Integer> idsD) throws SQLException {
        initComponents();
        modeloT1=(DefaultTableModel) jTable1.getModel();
         modeloT2=(DefaultTableModel) jTable2.getModel(); 
         modeloT3=(DefaultTableModel) jTable3.getModel(); 
         modeloT4=(DefaultTableModel) jTable4.getModel(); 
        
          
        this.datos2=datos2;
        
        
        this.idsN=idsN;
        
        this.idsM=idsM;
        
        this.idsD=idsD;
        
        for(int i=0;i<idsN.size();i++){
            for(int j=(i+1);j<idsN.size();j++){
            if(idsN.elementAt(i) == idsN.elementAt(j)){
                idsN.remove(j);
            }
            }
        }
        
        
        
         System.out.println("Tamaño Datos:: "+datos2.size()+" Datos N:: "+idsN.size()+" Datos M:: "+idsM.size() +" Datos D::"+idsD.size());
        
        for(int i=0;i<datos2.size();i++){
        modeloT1.addRow(new Object[]{"","","","",""});
       
      
       
        }
        
        for(int i=0;i<datos2.size();i++){
          modeloT2.addRow(new Object[]{"","","","",""});   
    }
         for(int i=0;i<datos2.size();i++){
          modeloT3.addRow(new Object[]{"","","","",""});   
    }
          for(int i=0;i<datos2.size();i++){
          modeloT4.addRow(new Object[]{"","","","",""});   
    }
        
        
        
        
        datos();
        if(!idsN.isEmpty()){
            System.out.println("Consultar padres");
         consulpadres();
         
        }
        if(!idsM.isEmpty()){
         matrimonio();
         familiares();
        }
        if(!idsD.isEmpty()){
       
        familiaresD();
        }
        
       testigos(); 
       
       limpiar();
       
       if(idsN.size()>0)
       limpiarN();
       
       if(idsM.size()>0)
           limpiarM();
       
       if(idsD.size()>0)
           limpiarD();
      // modeloT1.removeRow(3);
        
      lim_tabla();
       JOptionPane.showMessageDialog(null,"Se obtuvieron "+modeloT1.getRowCount()+" resultados","INFORMACION",JOptionPane.INFORMATION_MESSAGE);
      //if(modeloT1.getRowCount()==40)
     //     jButton2.setEnabled(false);
    }

    public void lim_tabla(){
        int tabla2=modeloT2.getRowCount();
        int tabla3=modeloT3.getRowCount();
        int tabla4=modeloT4.getRowCount();
        
        int i=0,j=0,k=0,cont=0, filast2=modeloT2.getRowCount();
    /*for(int i=0;i<tabla2;i++){
    if(modeloT2.getValueAt(i,0).toString().equals("")){
        modeloT2.removeRow(i);
    }
    }
    */
          
         while(i<modeloT2.getRowCount()){
             if(modeloT2.getValueAt(i,0).toString().equals("")){
             modeloT2.removeRow(i);
             i=0;
             }
             else{
             i++;
             }
             
             
         }
         
         while(k<modeloT3.getRowCount()){
             if(modeloT3.getValueAt(k,0).toString().equals("")){
             modeloT3.removeRow(k);
             k=0;
             }
             else{
             k++;
             }
            
             
         }
         
         
         
         while(j<modeloT4.getRowCount()){
             if(modeloT4.getValueAt(j,0).toString().equals("")){
             modeloT4.removeRow(j);
             j=0;
             }
             else{
             j++;
             }
             
             
         }
             
             
          //  i++;      
           // cont++;
           
    
    
   /* 
    
     for(int j=0;j<tabla3;j++){
    if(modeloT3.getValueAt(j,1).toString().equals(""))
        modeloT3.removeRow(j);
    }
        
       
      for(int k=0;k<tabla4;k++){
    if(modeloT4.getValueAt(k,0).toString().equals(""))
        modeloT4.removeRow(k);
    }
     */   
    }
    
    
    private Nacimiento_consul() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    public int buscar(){
    int id=-1;
    if(modeloT1.getRowCount() >0){
        
    for(int j=0;j<modeloT1.getRowCount();j++ ){
        for(int k=(j+1);k<modeloT1.getRowCount();k++ ){
           
            if(modeloT1.getValueAt(j, 0).toString().equals(modeloT1.getValueAt(k, 0).toString()) ){
            id=k;
            break;      
            }
        }
    }
}
    return id;
    }
    
    public void limpiar(){
for(int i=0;i<modeloT1.getRowCount();i++){
int id=buscar();
   if(id>=0){
    modeloT1.removeRow(id);
   
   }
   else{
   break;
   }
}  
    }
    
    
       public int buscarN(){
    int id=-1;
    if(modeloT2.getRowCount() >0){
        
    for(int j=0;j<modeloT2.getRowCount();j++ ){
        for(int k=(j+1);k<modeloT2.getRowCount();k++ ){
           
            if(modeloT2.getValueAt(j, 0).toString().equals(modeloT2.getValueAt(k, 0).toString()) ){
            id=k;
            break;      
            }
        }
    }
}
    return id;
    }
    
    public void limpiarN(){
for(int i=0;i<modeloT2.getRowCount();i++){
int id=buscarN();
   if(id>=0){
    modeloT2.removeRow(id);
   
   }
   else{
   break;
   }
}  
    }
    
           public int buscarM(){

    int id=-1;
    if(modeloT3.getRowCount() >0){
        
    for(int j=0;j<modeloT3.getRowCount();j++ ){
        for(int k=(j+1);k<modeloT3.getRowCount();k++ ){
           
            if(modeloT3.getValueAt(j, 0).toString().equals(modeloT3.getValueAt(k, 0).toString()) ){
            id=k;
            break;      
            }
        }
    }
}
    return id;
    }
    
    public void limpiarM(){
for(int i=0;i<modeloT3.getRowCount();i++){
int id=buscarM();
   if(id>=0){
    modeloT3.removeRow(id);
   
   }
   else{
   break;
   }
}  
    }
    
     public int buscarD(){

    int id=-1;
    if(modeloT4.getRowCount() >0){
        
    for(int j=0;j<modeloT4.getRowCount();j++ ){
        for(int k=(j+1);k<modeloT4.getRowCount();k++ ){
           
            if(modeloT4.getValueAt(j, 0).toString().equals(modeloT4.getValueAt(k, 0).toString()) ){
            id=k;
            break;      
            }
        }
    }
}
    return id;
    }
    
    public void limpiarD(){
for(int i=0;i<modeloT4.getRowCount();i++){
int id=buscarD();
   if(id>=0){
    modeloT4.removeRow(id);
   
   }
   else{
   break;
   }
}  
    }
    
       
        
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable4 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Nacimiento"));

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Acta", "Nombre", "Padres"
            }
        ));
        jTable2.setRowHeight(25);
        jScrollPane2.setViewportView(jTable2);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 465, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 264, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Registro"));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Acta", "Año", "Libro", "Estado", "Municipio", "Fecha", "Testigos", "Observaciones"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1226, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Matrimonio"));

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Acta", "Contrayentes", "Tipo de acta", "Familiares"
            }
        ));
        jScrollPane3.setViewportView(jTable3);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Defunciones"));
        jPanel4.setToolTipText("");

        jTable4.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Acta", "Finado", "Causa", "Fecha certificado", "Familiares"
            }
        ));
        jScrollPane4.setViewportView(jTable4);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(177, 177, 177))
        );

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton1.setText("Generar excel");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(69, 69, 69)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(145, 145, 145))
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(601, 601, 601)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       /*La ruta donde se creará el archivo*/
       
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-HH;mm;ss");
                 java.util.Date date = new java.util.Date();
               System.out.println(dateFormat.format(date)  );
                  
       String filenom="/Consulta "+String.valueOf(dateFormat.format(date))+".xls";
System.out.println(filenom);
        String rutaArchivo = System.getProperty("user.home")+filenom;
        /*Se crea el objeto de tipo File con la ruta del archivo*/
        File archivoXLS = new File(rutaArchivo);
        /*Si el archivo existe se elimina*/
        if(archivoXLS.exists()) archivoXLS.delete();
      try {
          /*Se crea el archivo*/
          archivoXLS.createNewFile();
     
        
        /*Se crea el libro de excel usando el objeto de tipo Workbook*/
        Workbook libro = new HSSFWorkbook();
        /*Se inicializa el flujo de datos con el archivo xls*/
        FileOutputStream archivo;
      
          archivo = new FileOutputStream(archivoXLS);
    
        
        /*Utilizamos la clase Sheet para crear una nueva hoja de trabajo dentro del libro que creamos anteriormente*/
        org.apache.poi.ss.usermodel.Sheet hoja = libro.createSheet("Datos Acta");
        
        org.apache.poi.ss.usermodel.Sheet hoja2 = libro.createSheet("Actas de nacimiento");
        org.apache.poi.ss.usermodel.Sheet hoja3 = libro.createSheet("Actas de matrimonio");
        org.apache.poi.ss.usermodel.Sheet hoja4 = libro.createSheet("Actas de defuncion");
        
        
        /*Hacemos un ciclo para inicializar los valores de 10 filas de celdas*/
        for(int f=0;f<=modeloT1.getRowCount();f++){
            /*La clase Row nos permitirá crear las filas*/
            Row fila = hoja.createRow(f);
            
            /*Cada fila tendrá 5 celdas de datos*/
            for(int c=0;c<modeloT1.getColumnCount();c++){
                /*Creamos la celda a partir de la fila actual*/
                Cell celda = fila.createCell(c);
                
                /*Si la fila es la número 0, estableceremos los encabezados*/
                if(f==0){
                    celda.setCellValue(modeloT1.getColumnName(c));
                    
                }else{
                    /*Si no es la primera fila establecemos un valor*/
                    celda.setCellValue(modeloT1.getValueAt((f-1), c).toString());
                }
            }
        }
        
        
  for(int f=0;f<=modeloT2.getRowCount();f++){
            /*La clase Row nos permitirá crear las filas*/
            Row fila = hoja2.createRow(f);
            
            /*Cada fila tendrá 5 celdas de datos*/
            for(int c=0;c<modeloT2.getColumnCount();c++){
                /*Creamos la celda a partir de la fila actual*/
                Cell celda = fila.createCell(c);
                
                /*Si la fila es la número 0, estableceremos los encabezados*/
                if(f==0){
                    celda.setCellValue(modeloT2.getColumnName(c));
                    
                }else{
                    /*Si no es la primera fila establecemos un valor*/
                    celda.setCellValue(modeloT2.getValueAt((f-1), c).toString());
                }
            }
        }
  
    for(int f=0;f<=modeloT3.getRowCount();f++){
            /*La clase Row nos permitirá crear las filas*/
            Row fila = hoja3.createRow(f);
            
            /*Cada fila tendrá 5 celdas de datos*/
            for(int c=0;c<modeloT3.getColumnCount();c++){
                /*Creamos la celda a partir de la fila actual*/
                Cell celda = fila.createCell(c);
                
                /*Si la fila es la número 0, estableceremos los encabezados*/
                if(f==0){
                    celda.setCellValue(modeloT3.getColumnName(c));
                    
                }else{
                    /*Si no es la primera fila establecemos un valor*/
                    celda.setCellValue(modeloT3.getValueAt((f-1), c).toString());
                }
            }
        }
    
      for(int f=0;f<=modeloT4.getRowCount();f++){
            /*La clase Row nos permitirá crear las filas*/
            Row fila = hoja4.createRow(f);
            
            /*Cada fila tendrá 5 celdas de datos*/
            for(int c=0;c<modeloT4.getColumnCount();c++){
                /*Creamos la celda a partir de la fila actual*/
                Cell celda = fila.createCell(c);
                
                /*Si la fila es la número 0, estableceremos los encabezados*/
                if(f==0){
                    celda.setCellValue(modeloT4.getColumnName(c));
                    
                }else{
                    /*Si no es la primera fila establecemos un valor*/
                    celda.setCellValue(modeloT4.getValueAt((f-1), c).toString());
                }
            }
        }

        
        /*Escribimos en el libro*/
        libro.write(archivo);
        /*Cerramos el flujo de datos*/
        archivo.close();
        /*Y abrimos el archivo con la clase Desktop*/
        Desktop.getDesktop().open(archivoXLS);
    
        } catch (Exception ex) {
          Logger.getLogger(Nacimiento_consul.class.getName()).log(Level.SEVERE, null, ex);
      }
      

       
       
    }//GEN-LAST:event_jButton1ActionPerformed

    @Override
        public int print(Graphics graf, PageFormat pagefor, int pageIndex) throws PrinterException {
            int inc=0,inc2=0;
            int x=30,y=40;
            //int f=0,c=0;
            graf.setFont(new Font( "Calibri", Font.PLAIN, 11 ));
 
            
            graf.drawString(modeloT1.getColumnName(0), 30, 30); //c1
            graf.drawString(modeloT1.getColumnName(1), 90, 30); //c2
            graf.drawString(modeloT1.getColumnName(2), 150, 30); //c3
            graf.drawString(modeloT1.getColumnName(3), 210, 30); //c4
            graf.drawString(modeloT1.getColumnName(4), 320, 30); //c5
            graf.drawString(modeloT1.getColumnName(5), 450, 30); //c6
            graf.drawString(modeloT1.getColumnName(6), 530, 30); //c7
            graf.drawString(modeloT1.getColumnName(7), 700, 30); //c8
            
                    for(int f=0;f<modeloT1.getRowCount();f++){
                    for(int c=0;c<modeloT1.getColumnCount();c++){  
                         
                        if(c==0)
                        graf.drawString(modeloT1.getValueAt(f, c).toString(), 30, (y+inc));
                        if(c==1)
                        graf.drawString(modeloT1.getValueAt(f, c).toString(), 90, (y+inc));
                         if(c==2)
                        graf.drawString(modeloT1.getValueAt(f, c).toString(), 150, (y+inc));
                        if(c==3)
                        graf.drawString(modeloT1.getValueAt(f, c).toString(), 210, (y+inc));
                         if(c==4)
                        graf.drawString(modeloT1.getValueAt(f, c).toString(), 320, (y+inc));
                        if(c==5)
                        graf.drawString(modeloT1.getValueAt(f, c).toString(), 450, (y+inc));
                         if(c==6)
                        graf.drawString(modeloT1.getValueAt(f, c).toString(), 530, (y+inc));
                        if(c==7)
                        graf.drawString(modeloT1.getValueAt(f, c).toString(), 700, (y+inc));
                        
                        
                        
                        
                       // System.out.println(modeloT1.getValueAt(f, c).toString());
                         
                     
                    
                    }
                  
                    inc+=12;
                    }
               
            
            
    return PAGE_EXISTS;
    }
    
    
    
    
    
    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Nacimiento_consul.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Nacimiento_consul.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Nacimiento_consul.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Nacimiento_consul.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Nacimiento_consul().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    private javax.swing.JTable jTable4;
    // End of variables declaration//GEN-END:variables
public void datos(){
    int cont=0;
    for(int i=0;i<datos2.size();i++){

        System.out.println(String.valueOf(datos2.elementAt(i).getacta()));
        System.out.println(String.valueOf(datos2.elementAt(i).getNombre()));       
   
    modeloT1.setValueAt(datos2.elementAt(i).getacta(), i,0);
modeloT1.setValueAt(datos2.elementAt(i).getaño(), i,1);
modeloT1.setValueAt(datos2.elementAt(i).getlibro(), i,2);
modeloT1.setValueAt(datos2.elementAt(i).getestado(), i,3);
modeloT1.setValueAt(datos2.elementAt(i).getmuni(), i,4);
modeloT1.setValueAt(datos2.elementAt(i).getfecha(), i,5);
modeloT1.setValueAt(datos2.elementAt(i).getobs(), i,7);
System.out.println("##"+i);
cont++;
  
if(!idsN.isEmpty()){
    for(int k=0;k<idsN.size();k++){
if( Integer.parseInt(datos2.elementAt(i).getacta() ) ==  idsN.elementAt(k)  ){
 modeloT2.setValueAt(datos2.elementAt(i).getacta(), i,0);   //agrego las actas a  la tabla 2
 modeloT2.setValueAt(datos2.elementAt(i).getNombre(), i, 1); //agrego el nombre de la ersona a la tabla 2
}
}
}

if(!idsM.isEmpty()){
if( Integer.parseInt(datos2.elementAt(i).getacta() ) ==  idsM.elementAt(i)  ){
 modeloT3.setValueAt(datos2.elementAt(i).getacta(), i,0);   //agrego las actas a  la tabla 2
 if( Integer.parseInt(datos2.elementAt(i).gettipoA())    ==1       ){
modeloT3.setValueAt("DISPENSA", i, 2);
}
if(Integer.parseInt(datos2.elementAt(i).gettipoA())==2){
modeloT3.setValueAt("SEÑALAMINETO", i, 2);
}
if(Integer.parseInt(datos2.elementAt(i).gettipoA())==3){
modeloT3.setValueAt("PRESENTACION", i, 2);
}
if(Integer.parseInt(datos2.elementAt(i).gettipoA())==4){
modeloT3.setValueAt("CELEBRACION", i, 2);
}
 
}}

if(!idsD.isEmpty()){
for(int k=0;k<idsD.size();k++){
if( Integer.parseInt(datos2.elementAt(i).getacta() ) ==  idsD.elementAt(k)  ){
 modeloT4.setValueAt(datos2.elementAt(i).getacta(), i,0);   //agrego las actas a  la tabla 2
 modeloT4.setValueAt(datos2.elementAt(i).getnombreF(), i, 1); //agrego el nombre de la ersona a la tabla 2
  modeloT4.setValueAt(datos2.elementAt(i).getcausa(), i, 2);
   modeloT4.setValueAt(datos2.elementAt(i).getfechac(), i, 3); 
            }
        }
       }


    }
}

public void consulpadres() throws SQLException{
    int i=0,j=0;
conexion=new Conexion();
        Connection con=conexion.getConection();
         Statement st;
         ResultSet rs2=null;
         st=con.createStatement();
         System.out.println("*****");
         rs2=st.executeQuery("SELECT * FROM datos LEFT JOIN progenitor ON datos.id_datos=progenitor.datos_id_datos WHERE progenitor.acta_num_acta IS NOT NULL ORDER BY progenitor.acta_num_acta ASC ");
        
        for(int l=0;l<idsN.size();l++){
           
         while(rs2.next()){
             
           // System.out.println("Voy a comparar");
             if(     rs2.getInt(6 ) == Integer.parseInt(String.valueOf(modeloT1.getValueAt(l, 0)   ))   ){
                 dads.addElement(rs2.getString(2).toUpperCase() +" "+ rs2.getString(3).toUpperCase()+" "+ rs2.getString(4).toUpperCase() );             
                 
//rtyrtyr
             System.out.println(">>>"+dads);
             }
             if(rs2.isLast()){               
            modeloT2.setValueAt(String.valueOf(dads), j, 2);     
            System.out.println("Agregue"+dads);
             break;
             }        
         }
             j++;
             dads.removeAllElements();
          //
            rs2.first();                       
         }
        
         rs2.close();
         con.close();
}

public void matrimonio() throws SQLException{
int i=0;
conexion=new Conexion();
        Connection con=conexion.getConection();
         Statement st;
         ResultSet rs2=null;
         st=con.createStatement();
         rs2=st.executeQuery("SELECT * FROM persona INNER JOIN dat_act_mat  ON persona.acta_num_acta=dat_act_mat.acta_num_acta ORDER BY dat_act_mat.acta_num_acta ASC");
         while(rs2.next()){
             
             if(     rs2.getInt(5 ) == Integer.parseInt(String.valueOf(modeloT3.getValueAt(i, 0)   ))   ){
                 
                 mat.add(rs2.getString(2).toUpperCase() +" "+ rs2.getString(3).toUpperCase()+" "+ rs2.getString(4).toUpperCase()+ "," );             
             //rtyrtyr
             }
             if(rs2.getInt(5 ) != Integer.parseInt(String.valueOf(modeloT3.getValueAt(i, 0)   ))   || rs2.isLast()){
                 System.out.println("-->"+mat);
                  modeloT3.setValueAt(mat, i, 1);
                  if(datos2.size()>(i+1)){
                      i++;
                  }
                  }          
        
            
            
         }
}


public void familiares()throws SQLException{
int i=0;
int j=0;
conexion=new Conexion();
        Connection con=conexion.getConection();
         Statement st;
         ResultSet rs2=null;
         st=con.createStatement();
         rs2=st.executeQuery("SELECT * FROM datos LEFT JOIN familiares ON datos.id_datos=familiares.datos_id_datos WHERE familiares.acta_num_acta IS NOT NULL ORDER BY familiares.acta_num_acta ASC ");          
            for(int l=0;l<idsM.size();l++){
           
         while(rs2.next()){
             
           // System.out.println("Voy a comparar");
             if(     rs2.getInt(6 ) == Integer.parseInt(String.valueOf(modeloT1.getValueAt(l, 0)   ))   ){
                 famat.addElement(rs2.getString(2).toUpperCase() +" "+ rs2.getString(3).toUpperCase()+" "+ rs2.getString(4).toUpperCase() );             
                 
//rtyrtyr
             System.out.println(">>>"+famat);
             }
             if(rs2.isLast()){               
            modeloT3.setValueAt(String.valueOf(famat), j, 3);     
            System.out.println("Agregue"+famat);
             break;
             }        
         }
             j++;
             famat.removeAllElements();
          //
            rs2.first();                       
         }
      //   }
}

public void familiaresD()throws SQLException{
int i=0;
int j=0;
conexion=new Conexion();
        Connection con=conexion.getConection();
         Statement st;
         ResultSet rs2=null;
         st=con.createStatement();
         rs2=st.executeQuery("SELECT * FROM datos LEFT JOIN familiares ON datos.id_datos=familiares.datos_id_datos WHERE familiares.acta_num_acta IS NOT NULL ORDER BY familiares.acta_num_acta ASC ");
        for(int l=0;l<datos2.size();l++){
           
         while(rs2.next()){
             
           // System.out.println("Voy a comparar");
             if(     rs2.getInt(6 ) == Integer.parseInt(String.valueOf(modeloT1.getValueAt(l, 0)   ))   ){
                 //System.out.println("Pase la comparacion");
                 famdef.addElement(rs2.getString(2).toUpperCase() +" "+ rs2.getString(3).toUpperCase()+" "+ rs2.getString(4).toUpperCase() );             
                 
//rtyrtyr
             System.out.println(">>>"+famdef);
             }
             if(rs2.isLast()){
               //for(int k=0;k<famdef.size();k++){
            modeloT4.setValueAt(String.valueOf(famdef), j, 4);
            //System.out.println("Ingresé->"+famdef);
              // }
             
             
             break;
             }
         
         }
             j++;
             famdef.removeAllElements();
          //
            rs2.first(); 
                       
         }
}


public void testigos()throws SQLException{
int i=0;
int j=0;
conexion=new Conexion();
        Connection con=conexion.getConection();
         Statement st;
         ResultSet rs2=null;
         st=con.createStatement();
         rs2=st.executeQuery("SELECT * FROM datos LEFT JOIN testigos ON datos.id_datos=testigos.datos_id_datos WHERE testigos.acta_num_acta IS NOT NULL ORDER BY testigos.acta_num_acta ASC "); 
         
          for(int l=0;l<datos2.size();l++){
           
         while(rs2.next()){
             
           // System.out.println("Voy a comparar");
             if(     rs2.getInt(6 ) == Integer.parseInt(String.valueOf(modeloT1.getValueAt(l, 0)   ))   ){
                 //System.out.println("Pase la comparacion");
                 testigoss.addElement(rs2.getString(2).toUpperCase() +" "+ rs2.getString(3).toUpperCase()+" "+ rs2.getString(4).toUpperCase() );             
                 
//rtyrtyr
             //System.out.println(">>>"+testigoss);
             }
             if(rs2.isLast()){
               //for(int k=0;k<famdef.size();k++){
            modeloT1.setValueAt(String.valueOf(testigoss), l, 6);
            //System.out.println("Ingresé->"+famdef);
              // }
             
             
             break;
             }
         
         }
             j++;
             testigoss.removeAllElements();
          //
            rs2.first(); 
                       
         }
}



}
