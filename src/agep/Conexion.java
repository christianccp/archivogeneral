/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agep;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
/**
 *
 * @author Christian
 */
public class Conexion {
   private static Connection conexion;
   /*private static String servidor="jdbc:mysql://192.168.1.127:3306/bd_agep";
   private static String usr="%";
   private static String pass="";*/
   
   private static String servidor="jdbc:mysql://10.21.29.175:3306/bd_agep";
   private static String usr="AGEP";
   private static String pass="agep";
    public Conexion(){
            try{
                
                Class.forName("com.mysql.jdbc.Driver");
                 conexion= DriverManager.getConnection(servidor,usr,pass);
                //System.out.println("Conexion lista");
        }catch(SQLException sqlexc){
                JOptionPane.showMessageDialog(null,"Error de conexión: "+sqlexc,"ERROR",JOptionPane.ERROR_MESSAGE);
                
            }
            catch(ClassNotFoundException cnfexc){
                JOptionPane.showMessageDialog(null,"Error de controlador","ERROR",JOptionPane.ERROR_MESSAGE);
            }
            
    }
    
           public Connection getConection(){
            return conexion;
                                            }
}
